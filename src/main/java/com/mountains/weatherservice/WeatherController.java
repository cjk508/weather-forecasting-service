package com.mountains.weatherservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import tk.plogitech.darksky.api.jackson.DarkSkyJacksonClient;
import tk.plogitech.darksky.forecast.*;
import tk.plogitech.darksky.forecast.model.Forecast;
import tk.plogitech.darksky.forecast.model.Latitude;
import tk.plogitech.darksky.forecast.model.Longitude;

import java.time.Instant;

@RestController
@Slf4j
public class WeatherController {
    private final APIKey apiKey;

    public WeatherController(APIKey apiKey) {
        this.apiKey = apiKey;
    }


    @GetMapping(
            path = "/weatherJackson",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public String getWeatherJackson() throws ForecastException, JsonProcessingException {
        ForecastRequest request = new ForecastRequestBuilder()
                .key(apiKey)
                .time(Instant.now())
                .location(new GeoCoordinates(new Longitude(13.377704), new Latitude(52.516275)))
                .build();
        log.info("Requesting data for {}", request);
        ObjectMapper mapper = new ObjectMapper();
        Forecast forecast = new DarkSkyJacksonClient().forecast(request);
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(forecast);
    }


    @GetMapping(
            path = "/weather",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public String getWeather() throws ForecastException, JsonProcessingException {
        ForecastRequest request = new ForecastRequestBuilder()
                .key(apiKey)
                .time(Instant.now())
                .location(new GeoCoordinates(new Longitude(13.377704), new Latitude(52.516275)))
                .build();
        log.info("Requesting data for {}", request);
        String forecastJsonString = new DarkSkyClient().forecastJsonString(request);
        ObjectMapper mapper = new ObjectMapper();
        Forecast f = mapper.readValue(forecastJsonString, Forecast.class);
        log.info("Retrieved data of {}", forecastJsonString);
        log.info("Retrieved data of {}", f);
        return forecastJsonString;
    }

}

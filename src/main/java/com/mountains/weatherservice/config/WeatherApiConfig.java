package com.mountains.weatherservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import tk.plogitech.darksky.forecast.APIKey;

@Configuration
public class WeatherApiConfig {

    @Bean
    public APIKey getWeatherApiKey(@Value("${weather.api.key}") String apiKey) {
        return new APIKey(apiKey);
    }
}
